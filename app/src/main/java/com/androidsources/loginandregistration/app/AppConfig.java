package com.androidsources.loginandregistration.app;


public class AppConfig {

	/*
	This is the only place where you should specify the address and the port for the server.
	 */
	private static String urlbase = "http://130.233.42.180:8080/";
	//private static String urlbase = "http://192.168.0.120:3000/";
	// Server login url
	public static String URL_LOGIN = urlbase + "api/auth/";

	// Server register url
	public static String URL_REGISTER = urlbase + "api/user/";

	// Server get all events url string
	public static String URL_EVENTS = urlbase + "api/events";


	/*
        Use this method to form a url needed to make PUT request to modify existing event
         */
	public static String getUrlEventEdit(String id){
		String myUrl = urlbase + "api/events/" + id + "/edit";
		return myUrl;
	}

	/*
    Use this method to form a url string needed to perform POST request to server to create new event
     */
	public static String getUrlEventCreate(){
		String myUrl = urlbase + "api/events/new";
		return myUrl;
	}

	/*
    Use this method to form a URL needed to send a DEL request to the server to delete existing event
     */
	public static String getUrlEventDelete(String id){
		String myUrl = urlbase + "api/events/" + id + "/edit";
		return myUrl;
	}

	public static String getUrlBase(){
		return urlbase;
	}
}
