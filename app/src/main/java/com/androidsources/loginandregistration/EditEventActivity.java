package com.androidsources.loginandregistration;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewDebug;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidsources.loginandregistration.app.AppConfig;
import com.androidsources.loginandregistration.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class EditEventActivity extends Activity {
    private Button buttonCreate = null;
    private EditText event_name_input = null;
    private EditText event_date_input = null;
    private EditText event_time_input = null;
    private EditText event_location_input = null;
    private EditText event_priority_input = null;
    private Spinner event_availability_input = null;
    private Switch event_repeat_input = null;
    private Boolean errorDate = false;
    private Boolean errorTime = false;

    private String idToEdit = null;


    private String __name = null;
    private String __date = null;
    private String __time = null;
    private String __location = null;
    private String __priority = null;
    private String __availability = null;
    Boolean __repeat = false;
    // TODO: make status proper
    //String name, String loc, Boolean repeat, String sts, String priority, String availability, String date, String time


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        event_name_input = (EditText)findViewById(R.id.event_name_input);
        event_date_input = (EditText)findViewById(R.id.event_date_input);
        event_time_input = (EditText)findViewById(R.id.event_time_input);
        event_location_input = (EditText)findViewById(R.id.event_location_input);
        event_priority_input = (EditText)findViewById(R.id.event_priority_input);
        event_availability_input = (Spinner)findViewById(R.id.event_availability_input);
        event_repeat_input = (Switch)findViewById(R.id.event_repeat_input);
        buttonCreate = (Button) findViewById(R.id.buttonCreate);


        Intent intent = getIntent();
        idToEdit = intent.getStringExtra("id");

        // find such event with this id:
        Event updatingEvent = getEventById(idToEdit);

        event_name_input.setText(updatingEvent.get__name());
        event_location_input.setText(updatingEvent.get__location());
        event_date_input.setText(getDateAsString(updatingEvent.get__date()));
        event_time_input.setText(getTimeAsString(updatingEvent.get__date()));
        //SpinnerAdapter adapt = event_availability_input.getAdapter();
        event_priority_input.setText(Integer.toString(updatingEvent.get__prioroity()));
        event_repeat_input.setChecked(false);

        event_date_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!event_date_input.getText().toString().matches("([0-3][0-9]?\\.[0-1][0-9]?\\.[2][0][0-9][0-9])")) {
                    errorDate = true;
                } else {
                    errorDate = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        event_time_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!event_time_input.getText().toString().matches("([0-2][0-9]\\:[0-5][0-9])"))
                {
                    errorTime = true;
                } else {
                    errorTime = false;
                }

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        });


        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInfo()){
                    if (errorDate){
                        // Show error message
                        Toast.makeText(getApplicationContext(),
                                "Date must be in dd.MM.yyyy", Toast.LENGTH_LONG).show();
                    } else if (errorTime){
                        // Show error message
                        Toast.makeText(getApplicationContext(),
                                "Time must be in HH:MM", Toast.LENGTH_LONG).show();
                    }else {
                        // send request and add new Event into singleton.
                        __name = event_name_input.getText().toString();
                        __date = event_date_input.getText().toString();
                        __time = event_time_input.getText().toString();
                        __location = event_location_input.getText().toString();
                        __priority = event_priority_input.getText().toString();
                        __availability = event_availability_input.getSelectedItem().toString();
                        __repeat = event_repeat_input.isSelected();
                        // TODO: make status proper
                        //String name, String loc, Boolean repeat, String sts, String priority, String availability, String date, String time
                        Date myDate = getDateFromDateTimeStrings(__date, __time);
                        sendPutRequest(AppConfig.getUrlEventEdit(idToEdit), "edit_event", makeJsonString(__name, __location, __repeat, "upcoming", __priority, __availability, myDate.toString(), __time));
                    }
                } else {
                    // Show error message
                    Toast.makeText(getApplicationContext(),
                            "Not all fields are valid", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private Event getEventById(String id){
        for (Event ev: EventsHolder.getInstance().getData()){
            if (ev.get__id().equals(id)){
                return ev;
            }
        }
        return EventsHolder.getInstance().getData().get(1); // bad code here
    }

    private Boolean validateInfo(){
        if (event_name_input.length() < 2) return false;
        if (event_location_input.length() < 3) return false;
        if (event_date_input.length() < 10) return false;
        if (event_time_input.length() < 5) return false;
        if (event_priority_input.length() < 1) return false;
        if (!isInteger(event_priority_input.getText().toString())) return false;
        return true;
    }

    private boolean isInteger( String input ) {
        try {
            int val = Integer.parseInt( input );
            if ((val < 1) || (val >10) ) return false;
                else return true;
        }
        catch( Exception e ) {
            return false;
        }
    }

    private String getDateAsString(Date myDate){
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        return df.format(myDate);
    }

    private String getTimeAsString(Date myDate){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        return df.format(myDate);
    }

    private Date getDateFromDateTimeStrings(String myDate, String myTime){
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        try {
            return df.parse(myDate +" "+ myTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }


    private Date getDate(int day, int month, int year){
        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }

    private String makeJsonString(String name, String loc, Boolean repeat, String sts, String priority, String availability, String date, String time){
        String myRepeat = null;
        if (repeat) myRepeat = "true";
                else myRepeat = "false";
        return "{ \"name\":\"" + name +"\", \"loc\":\"" + loc +"\", \"date\":\"" + date +"\", \"time\":\"" +time+"\", \"repeat\":" + myRepeat +", \"sts\":\"" + sts+"\", \"priority\":" + priority +", \"availability\":\"" + availability + "\"}";
    }

    private Boolean updateExistingEvent(String id, String name, String loc, String sts, int priority, String availability, Date date){
        for (Event ev: EventsHolder.getInstance().getData()){
            if (ev.get__id().equals(id)){
                ev.set__name(name);
                ev.set__location(loc);
                ev.set__status(sts);
                ev.set__prioroity(priority);
                ev.set__availability(availability);
                ev.set__date(date);
                return true;
            }
        }
        return false;
    }


    private void sendPutRequest(String url, String action, String jSonData){
        String tag_jsonArray_req = action;
        System.out.println("CREATE URL:"+url);
        System.out.println("CREATE action:"+action);
        System.out.println("CREATE json:"+jSonData);
        // send request for retrieval all data
        JsonObjectRequest jsonObjectRequest = null;
        try {
            jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT,
                    url, new JSONObject(jSonData), new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    System.out.println("response"+ response.toString());
                    // parse response to retrieve name and date for each event
                    String message = null;
                    String deletedId = null;
                    try {
                        message = response.getString("message");
                        deletedId = response.getString("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                                e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                    Thread thread = new Thread(){
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(3500); // As I am using LENGTH_LONG in Toast
                                EditEventActivity.this.finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    //String id, String name, String loc, String sts, int priority, String availability, Date date
                    Date myDate = getDateFromDateTimeStrings(__date, __time);
                    Boolean res = updateExistingEvent(idToEdit, __name, __location, "upcoming", Integer.parseInt(__priority), __availability, myDate);
                    if (res) {
                        Toast.makeText(getApplicationContext(),
                                "Event " + deletedId + " " + message, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Event " + deletedId + " was not properly updated" + message, Toast.LENGTH_SHORT).show();
                    }
                    thread.start();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                            error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<String, String>();
                    //retrieve token and put it into request
                    String token = AppController.getString(getApplicationContext(), "token");
                    params.put("x-auth", token);
                    return params;
                }

            };
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    e.getMessage(), Toast.LENGTH_LONG).show();
        }

        // Adding request to  queue
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_jsonArray_req);
    }
}
