package com.androidsources.loginandregistration;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidsources.loginandregistration.app.AppConfig;
import com.androidsources.loginandregistration.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class ListDayEvents extends Activity {
    private ArrayList<String> ids = null; // Ids will be stored here.
    private Button delete_button = null;
    private Button edit_button = null;
    private int selectedPosition = -1;
    static final int REQUEST_EXIT = 2;
    static final int RESULT_OK = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_events);
        delete_button = (Button) findViewById(R.id.delete_button);
        edit_button = (Button) findViewById(R.id.edit_button);

        Intent intent = getIntent();
        int thisDay = intent.getIntExtra("day", 0);
        int thisMonth = intent.getIntExtra("month", 0);
        int thisYear = intent.getIntExtra("year", 0);

        // Select events that are only for this day and turn them into strings.
        ArrayList<Event> allTodayEvents = getThisDayEvents(thisDay, thisMonth, thisYear);
        ids = getIdsFromEvents(allTodayEvents); // contains ids
        ArrayList<String> events_strings = getStringsFromEvents(allTodayEvents);

        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.activity_day_events_listview, events_strings);
        final ListView listView = (ListView) findViewById(R.id.day_events_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                //String selectedFromList = (String) (listView.getItemAtPosition(myItemInt));
                selectedPosition = myItemInt;
                System.out.println("POS:" + selectedPosition);
            }
        });

        delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPosition != -1) {
                    String idToDel = ids.get(selectedPosition);
                    System.out.println("Deleting event with id:" + idToDel);
                    sendDelRequest(idToDel);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Nothing selected", Toast.LENGTH_SHORT).show();
                }
            }
        });

        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPosition != -1){
                    String idToEdit = ids.get(selectedPosition);
                    System.out.println("EDITING event with id:"+idToEdit);
                    openEditActivity(idToEdit);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Nothing selected", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("RESULT RECEIVED YEAH ___________________________");
        System.out.println("resultCode ___________________________"+resultCode);
        System.out.println("requestCode ___________________________"+requestCode);
        if (requestCode == REQUEST_EXIT) {
            if (resultCode == RESULT_OK) {
                System.out.println("______---- I SHOULD FINISH ---_______");
                ListDayEvents.this.finish();
            }
        }
    }

    private void openEditActivity (String id){
        Intent intent = new Intent(ListDayEvents.this, EditEventActivity.class);
        intent.putExtra("id", id);
        startActivityForResult(intent, REQUEST_EXIT);
    }


    private void sendDelRequest(String id){
        String tag_jsonArray_req = "deleting";
        String url = AppConfig.getUrlEventDelete(id);
        System.out.println("CREATE URL:"+ url);
        JsonObjectRequest jsonObjectRequest = null;
        try {
            jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE,
                    url, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    System.out.println("response"+ response.toString());
                    String message = null;
                    String deletedId = null;
                    try {
                        message = response.getString("message");
                        deletedId = response.getString("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                                e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                    Thread thread = new Thread(){
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(3500); // As I am using LENGTH_LONG in Toast
                                ListDayEvents.this.finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    deleteExistingEvent(deletedId);
                    Toast.makeText(getApplicationContext(),
                    "Event " + deletedId + " " + message, Toast.LENGTH_SHORT).show();
                    thread.start();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                            "Sorry, "+error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<String, String>();
                    //retrieve token and put it into request
                    String token = AppController.getString(getApplicationContext(), "token");
                    params.put("x-auth", token);
                    return params;
                }

            };
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    e.getMessage(), Toast.LENGTH_LONG).show();
        }

        // Adding request to  queue
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_jsonArray_req);
    }


    private void deleteExistingEvent(String id){
        EventsHolder.getInstance().deleteEvent(id);
    }


    private ArrayList<Event> getThisDayEvents(int thisDay, int month, int thisYear){
        ArrayList<Event> eventsToReturn = new ArrayList<Event>();

        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DAY_OF_MONTH, thisDay);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, thisYear);
        Date today = calendar.getTime();

        for (Event ev : EventsHolder.getInstance().getData()) {
            System.out.println("*******Persistant event:"+ev.toShortString());
            if (removeTime(ev.get__date()).equals(removeTime(today))){
                // Add the event to the list of the events neccessary to be shown:

                eventsToReturn.add(ev);
            }
        }
        return eventsToReturn;
    }


    private ArrayList<String> getStringsFromEvents(ArrayList<Event> myList){
        ArrayList<String> stringsToReturn = new ArrayList<String>();
        for (Event ev : myList){
            stringsToReturn.add(ev.toShortString());
        }
        return stringsToReturn;
    }

    private ArrayList<String> getIdsFromEvents(ArrayList<Event> myList){
        ArrayList<String> idsToReturn = new ArrayList<String>();
        for (Event ev : myList){
            idsToReturn.add(ev.get__id());
        }
        return idsToReturn;
    }

    private Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

}
