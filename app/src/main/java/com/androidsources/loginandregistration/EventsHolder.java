package com.androidsources.loginandregistration;


import java.util.ArrayList;

public class EventsHolder {
    private ArrayList<Event> eventsList = new ArrayList<Event>();

    public ArrayList<Event> getData() {
        return eventsList;
    }

    public void setEvents(ArrayList<Event> data) {
        this.eventsList = data;
    }

    public void deleteEvent(String id) {
        // search event with given id:
        int index = 0;
        //Boolean found = false;
        for (Event curEvent: this.eventsList){
            if(curEvent.get__id().equals(id)){
                // found the element, now we need to update it:
                this.eventsList.remove(index);
                break;
            }
            index += 1;
        }
    }

    public void addEvent(Event newEvent) {
        this.eventsList.add(newEvent);
    }

    public void updateEvent(String id, Event updatedEvent) {
        // search event with given id:
        int index = 0;
        //Boolean found = false;
        for (Event curEvent: this.eventsList){
            if(curEvent.get__id().equals(id)){
                // found the element, now we need to update it:
                this.eventsList.set(index, updatedEvent);
                break;
            }
            index += 1;
        }
    }

    public void eraseEvents(){
        this.eventsList.clear();
    }

    private static final EventsHolder holder = new EventsHolder();

    public static EventsHolder getInstance() {
        return holder;
    }
}
