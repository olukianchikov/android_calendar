package com.androidsources.loginandregistration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListAllEvents extends Activity {
    // Array of events

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_events);

        Intent intent = getIntent();
        ArrayList<String> events_strings = intent.getStringArrayListExtra("events");

        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.activity_events_listview, events_strings);
        ListView listView = (ListView) findViewById(R.id.events_list);
        listView.setAdapter(adapter);
    }
}