package com.androidsources.loginandregistration;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Lukianchikov on 03-Dec-15.
 */
public class Event {
    private String __id = null;
    private Date __date = null;
    private String __name = "";
    private String __user = "";
    private String __location = "";
    private int __prioroity = 9;
    private String __status = "";
    private String __availability ="";

    public Event(String __id, Date __date, String __name, String __user, String __location, int __prioroity, String __status, String __availability) {
        this.__id = __id;
        this.__date = __date;
        this.__name = __name;
        this.__user = __user;
        this.__location = __location;
        this.__prioroity = __prioroity;
        this.__status = __status;
        this.__availability = __availability;
    }

    public Event(Date __date, String __name, String __user, String __location) {
        this.__date = __date;
        this.__name = __name;
        this.__user = __user;
        this.__location = __location;
        this.__prioroity = 9;
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date dateToday = new Date();
        if(__date.after(dateToday)){
            this.__status = "upcoming";
        } else {
            this.__status = "past";
        }
        this.__availability = "busy";
    }

    @Override
    public String toString() {
        return "Event{" +
                "__id=" + __id +
                ", __date=" + __date +
                ", __name='" + __name + '\'' +
                ", __user='" + __user + '\'' +
                ", __location='" + __location + '\'' +
                ", __prioroity=" + __prioroity +
                ", __status='" + __status + '\'' +
                ", __availability='" + __availability + '\'' +
                '}';
    }

    public String toShortString() {
        return  __name + " " + "on " + __date + " in " + __location +
                " which is " + __status + " event.";
    }

    public String get__id() {
        return __id;
    }

    public void set__id(String __id) {
        this.__id = __id;
    }

    public Date get__date() {
        return __date;
    }

    public void set__date(Date __date) {
        this.__date = __date;
    }

    public String get__name() {
        return __name;
    }

    public void set__name(String __name) {
        this.__name = __name;
    }

    public String get__user() {
        return __user;
    }

    public void set__user(String __user) {
        this.__user = __user;
    }

    public String get__location() {
        return __location;
    }

    public void set__location(String __location) {
        this.__location = __location;
    }

    public int get__prioroity() {
        return __prioroity;
    }

    public void set__prioroity(int __prioroity) {
        this.__prioroity = __prioroity;
    }

    public String get__status() {
        return __status;
    }

    public void set__status(String __status) {
        this.__status = __status;
    }

    public String get__availability() {
        return __availability;
    }

    public void set__availability(String __availability) {
        this.__availability = __availability;
    }
}
