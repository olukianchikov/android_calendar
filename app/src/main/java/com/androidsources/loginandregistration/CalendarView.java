/*
 * Copyright (C) 2011 Chris Gao <chris@exina.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.androidsources.loginandregistration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.MonthDisplayHelper;
import android.view.MotionEvent;
import android.view.ViewDebug;
import android.widget.ImageView;

public class CalendarView extends ImageView {
    private static int WEEK_TOP_MARGIN = 74;
    private static int WEEK_LEFT_MARGIN = 40;
    private static int CELL_WIDTH = 58;
    private static int CELL_HEIGHT = 53;
    private static int CELL_MARGIN_TOP = 92;
    private static int CELL_MARGIN_LEFT = 239;
    private static float CELL_TEXT_SIZE;

    private Cell lastSelected = null;
    private int lastColor = 0;
    private static final String TAG = "CalendarView";
    private Calendar mRightNow = null;
    private Drawable mWeekTitle = null;
    private Cell mToday = null;
    private ArrayList<Cell> mEventDays = null;  // will store cells that have event days
    private Cell[][] mCells = new Cell[6][7];
    private OnCellTouchListener mOnCellTouchListener = null;
    MonthDisplayHelper mHelper;
    Drawable mDecoration = null;
    Drawable mEventDecoration = null;
    ArrayList<Drawable> mEventDecorationList = null;

    private ArrayList<Date> __allEvents = null;  // will store all users events

    /*
    Method to update all user events;
     */
    public void setAllEvents(ArrayList<Date> eventsList){
        __allEvents = new ArrayList<Date>(eventsList);
    }

    /*
    Method accepts integer values of year/month/day and returns
    true if the day has events, or false if doesn't.
     */
    public Boolean checkDayHasEvents(int day, int month, int year){
        if (__allEvents != null) {
            Calendar cal = Calendar.getInstance();
            for (Date aDay : __allEvents) {
                cal.setTime(aDay);
                int theYear = cal.get(Calendar.YEAR);
                int theMonth = cal.get(Calendar.MONTH);
                int theDay = cal.get(Calendar.DAY_OF_MONTH);
                if (theYear == year && theMonth == month && theDay == day) {
                    //Yes, the day has events
                    //System.out.println(theDay+"."+theMonth+"."+theYear+" MATCHES THE DAY "+day+"."+month+"."+year);
                    return true;
                }
            }
        }
        return false;
    }

    public interface OnCellTouchListener {
        public void onTouch(Cell cell);
    }

    public CalendarView(Context context) {
        this(context, null);
    }

    public CalendarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mDecoration = context.getResources().getDrawable(R.drawable.typeb_calendar_today);
        mEventDecoration = context.getResources().getDrawable(R.drawable.typeb_calendar_event);
        mEventDecorationList = new ArrayList<Drawable>();
        this.initCalendarView();
    }

    private void initCalendarView() {
        mEventDecorationList.clear(); // clear the list of elements with green circles.
        mRightNow = Calendar.getInstance();
        // prepare static vars
        Resources res = getResources();
        WEEK_TOP_MARGIN  = (int) res.getDimension(R.dimen.week_top_margin);
        WEEK_LEFT_MARGIN = (int) res.getDimension(R.dimen.week_left_margin);

        CELL_WIDTH = (int) res.getDimension(R.dimen.cell_width);
        CELL_HEIGHT = (int) res.getDimension(R.dimen.cell_height);
        CELL_MARGIN_TOP = (int) res.getDimension(R.dimen.cell_margin_top);
        CELL_MARGIN_LEFT = (int) res.getDimension(R.dimen.cell_margin_left);

        CELL_TEXT_SIZE = res.getDimension(R.dimen.cell_text_size);
        // set background
        setImageResource(R.drawable.background);
        mWeekTitle = res.getDrawable(R.drawable.calendar_week);

        mHelper = new MonthDisplayHelper(mRightNow.get(Calendar.YEAR), mRightNow.get(Calendar.MONTH));

    }

    /*
    Private method that builds all the cells (dates) in correct representation of the month.
     */
    private void initCells() {
        mEventDays = new ArrayList<Cell>();    // each time initCells invoked, the list of cells with events is created.
        mEventDecorationList.clear();
        class _calendar {
            public int day;
            public boolean thisMonth;
            public _calendar(int d, boolean b) {
                day = d;
                thisMonth = b;
            }
            public _calendar(int d) {
                this(d, false);
            }
        };
        _calendar tmp[][] = new _calendar[6][7];

        for(int i=0; i<tmp.length; i++) {
            int n[] = mHelper.getDigitsForRow(i);
            for(int d=0; d<n.length; d++) {
                if(mHelper.isWithinCurrentMonth(i,d))
                    tmp[i][d] = new _calendar(n[d], true);
                else
                    tmp[i][d] = new _calendar(n[d]);

            }
        }

        Calendar today = Calendar.getInstance();
        int thisDay = 0;
        mToday = null;
        if(mHelper.getYear()==today.get(Calendar.YEAR) && mHelper.getMonth()==today.get(Calendar.MONTH)) {
            thisDay = today.get(Calendar.DAY_OF_MONTH);
        }
        // build cells
        Rect Bound = new Rect(CELL_MARGIN_LEFT, CELL_MARGIN_TOP+(int)70, CELL_WIDTH+CELL_MARGIN_LEFT, CELL_HEIGHT+CELL_MARGIN_TOP);
        for(int week=0; week<mCells.length; week++) {
            for (int day = 0; day < mCells[week].length; day++) {
                if(tmp[week][day].thisMonth) {
                    if(day==0 || day==6 )
                        mCells[week][day] = new RedCell(tmp[week][day].day, new Rect(Bound), CELL_TEXT_SIZE);
                    else
                        mCells[week][day] = new Cell(tmp[week][day].day, new Rect(Bound), CELL_TEXT_SIZE);
                } else {
                    mCells[week][day] = new GrayCell(tmp[week][day].day, new Rect(Bound), CELL_TEXT_SIZE);
                }

                Bound.offset(CELL_WIDTH, 0); // move to next column

                if (__allEvents != null) {    // If we even have a list of events
                    //System.out.println("We have events!");
                    // get today --> And set bounds for the red circle:
                    int curMonth = mHelper.getMonth(); // from 0 to 11.
                    int curYear = mHelper.getYear();
                    //System.out.println("THIS MONTH IS:" + Integer.toString(curMonth));
                    //System.out.println("THIS Year IS:" + Integer.toString(curYear));
                    //System.out.println("Let us search if it contains day: "+ tmp[week][day].day);
                    //__allEvents.get(1).getDay();
                    Calendar cal = Calendar.getInstance();
                    if (tmp[week][day].day == thisDay && tmp[week][day].thisMonth) {
                        mToday = mCells[week][day];
                        mDecoration.setBounds(mToday.getBound());
                    } else if (tmp[week][day].thisMonth) {
                        for (Date aDay : __allEvents){
                            cal.setTime(aDay);
                            int theYear = cal.get(Calendar.YEAR);
                            int theMonth = cal.get(Calendar.MONTH);
                            int theDay = cal.get(Calendar.DAY_OF_MONTH);
                            if (theYear == curYear && theMonth ==curMonth && theDay ==tmp[week][day].day){
                                // the date should be present on the screen
                                //System.out.println("The Day " + Integer.toString(theDay) + "." + Integer.toString(theMonth) + "." + Integer.toString(theYear) + " should be drawn!");
                                mEventDays.add(mCells[week][day]);   // adding this day to list of days with green circles
                            }
                        }
                    }
                } else {
                    // get today --> And set bounds for the red circle:
                    if (tmp[week][day].day == thisDay && tmp[week][day].thisMonth) {
                        mToday = mCells[week][day];
                        mDecoration.setBounds(mToday.getBound());
                    }
                }
            }
            Bound.offset(0, CELL_HEIGHT); // move to next row and first column
            Bound.left = CELL_MARGIN_LEFT;
            Bound.right = CELL_MARGIN_LEFT+CELL_WIDTH;
        }
        // Let's now  decorate all the days in mEventDays:
        int i = 1;
        for (Cell p : mEventDays) {
            Drawable d = mEventDecoration.getConstantState().newDrawable();
            d.setBounds(p.getBound()); // getting the bounds of the last cell added to place a nice circle around it.
            mEventDecorationList.add(d);
            //System.out.println("Event "+Integer.toString(i)+": " + p.toString()+" decorated!");
        }
    }


    @Override
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Rect re = getDrawable().getBounds();
        WEEK_LEFT_MARGIN = CELL_MARGIN_LEFT = (right-left - re.width()) / 2 + 40;
        mWeekTitle.setBounds(WEEK_LEFT_MARGIN, WEEK_TOP_MARGIN, WEEK_LEFT_MARGIN+mWeekTitle.getMinimumWidth(), WEEK_TOP_MARGIN+mWeekTitle.getMinimumHeight());
        initCells();
        super.onLayout(changed, left, top, right, bottom);
    }

    public void setTimeInMillis(long milliseconds) {
        mRightNow.setTimeInMillis(milliseconds);
        initCells();
        this.invalidate();
    }

    public int getYear() {
        return mHelper.getYear();
    }

    public int getMonth() {
        return mHelper.getMonth();
    }

    /*
    Called when user clicks to go to the next month:
     */
    public void nextMonth() {
        lastSelected = null;
        lastColor = 0;
        mHelper.nextMonth();
        this.invalidate();      // redraw
        initCells();

    }

    public void refreshThisMonth() {
        lastSelected = null;
        lastColor = 0;
        this.invalidate();      // redraw
        initCells();
    }

    /*
    Called when user clicks to go to the last month:
     */
    public void previousMonth() {
        lastSelected = null;
        lastColor = 0;
        mHelper.previousMonth();
        this.invalidate();      // redraw
        initCells();
    }

    public boolean firstDay(int day) {
        return day==1;
    }

    public boolean lastDay(int day) {
        return mHelper.getNumberOfDaysInMonth()==day;
    }

    public void goToday() {
        Calendar cal = Calendar.getInstance();
        mHelper = new MonthDisplayHelper(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));
        initCells();
        invalidate();
    }

    public Calendar getDate() {
        return mRightNow;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(mOnCellTouchListener!=null){
            for(Cell[] week : mCells) {
                for(Cell day : week) {
                    if(day.hitTest((int)event.getX(), (int)event.getY())) {
                        if(!(day instanceof CalendarView.GrayCell)) {
                            if (lastSelected != null){
                                lastSelected.mPaint.setColor(lastColor);
                                lastSelected.draw();
                            }
                            lastSelected = day;
                            lastColor = day.mPaint.getColor();
                            day.mPaint.setColor(Color.BLUE);
                            //System.out.println("The color for the cell is set to be:" + String.valueOf(day.mPaint.getColor()));
                            day.draw();

                        }
                        this.invalidate();
                        mOnCellTouchListener.onTouch(day); // This day will go to onTouch in CalendarActivity
                    }
                }
            }
        }
        return super.onTouchEvent(event);
    }

    public void setOnCellTouchListener(OnCellTouchListener p) {
        mOnCellTouchListener = p;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // draw background
        super.onDraw(canvas);
        mWeekTitle.draw(canvas);

        // draw cells
        for(Cell[] week : mCells) {
            for(Cell day : week) {
                //System.out.println("Your Color of cell is: " + String.valueOf(day.mPaint.getColor()));
                day.draw(canvas);
            }
        }

        // draw today --> it draws a nice circle around.
        if(mDecoration!=null && mToday!=null) {
            mDecoration.draw(canvas);
        }

        // draw all the green circles:
        if(mEventDecorationList!=null) {
            System.out.println("Num of elements in DecorationList:"+Integer.toString(mEventDecorationList.size()));
            for (Drawable green: mEventDecorationList){
                //System.out.println("drawn");
                green.draw(canvas);
            }
        }

    }

    public class GrayCell extends Cell {
        public GrayCell(int dayOfMon, Rect rect, float s) {
            super(dayOfMon, rect, s);
            mPaint.setColor(Color.LTGRAY);
        }
    }

    private class RedCell extends Cell {
        public RedCell(int dayOfMon, Rect rect, float s) {
            super(dayOfMon, rect, s);
            mPaint.setColor(0xdddd0000);
        }
    }
}