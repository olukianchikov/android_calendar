package com.androidsources.loginandregistration;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

// Retrieve all functionality imports
import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.androidsources.loginandregistration.app.AppConfig;
import com.androidsources.loginandregistration.app.AppController;
import com.androidsources.loginandregistration.app.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //TextView token;
    Button btnLogout;
    Button getBtnRetrieveAll;

    private SessionManager session;

    // Arguments for "retrive all" functionality
    private ProgressDialog progressDialog;
    private DatePicker datePicker;
    private int year, month, day;
    private EditText dateFrom;
    private EditText dateTo;
    private Button btnRetrieveAll;
    //ADDED Start:
    private Button btnCalendar;
    //ADDED End
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set date format for "Retrieve all" functionality
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

       // token  = (TextView) findViewById(R.id.token);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnRetrieveAll = (Button) findViewById(R.id.btnRetrieveAll);

        //ADDED Start:
        btnCalendar = (Button) findViewById(R.id.btnCalendar);
        //ADDED End

        // "Retrieve all" related assignments
        dateFrom = (EditText) findViewById(R.id.date_from_input);
        dateFrom.setInputType(InputType.TYPE_NULL);
        dateTo = (EditText) findViewById(R.id.date_to_input);
        dateTo.setInputType(InputType.TYPE_NULL);
        setDateTimeField();

        // dialog poping up when requests being made
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        //setting toolbar
        Toolbar toolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);


        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        String token_str = AppController.getString(getApplicationContext(), "token");
        //token.setText(token_str);

        //ADDED Start:
        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar();
            }
        });
        //ADDED End

        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CalendarActivity.eraseEvents();
                logoutUser();
            }
        });

        btnRetrieveAll.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String date_from_str = dateFrom.getText().toString();
                String date_to_str = dateTo.getText().toString();
                String tag_jsonArray_req = "retrieve_events"; // meaningless, having no purpose
                String url = null;
                int method = 0;
                if ((date_from_str.length() == 0)&&(date_to_str.length() == 0)){
                    url = AppConfig.getUrlBase()+"api/events/";
                    method = Request.Method.GET;
                } else {
                    url = AppConfig.getUrlBase()+"api/events/range/" + date_from_str + "/to/" + date_to_str;
                    method = Request.Method.POST;
                }

                progressDialog.setMessage("Retrieving data ...");
                showDialog();

                // send request for retrieval all data
                JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(method,
                        url, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        hideDialog();
                        Log.w("response", response.toString());
                        // parse response to retrieve name and date for each event
                        ArrayList<String> allEvents = new ArrayList<String>();
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject event = null;
                            String name = null;
                            String date = null;
                            String location = null;
                            String event_desc = null;
                            try {
                                event = response.getJSONObject(i);
                                name = event.getString("name");
                                date = event.getString("date");
                                location = event.getString("loc");
                                event_desc = name + " " + date + " " + location;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            allEvents.add(event_desc);
                        }
                        // Launching other activity to list retrieved events
                        Intent intent = new Intent(MainActivity.this, ListAllEvents.class);
                        intent.putExtra("events", allEvents);
                        startActivity(intent);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();
                        hideDialog();
                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> params = new HashMap<String, String>();
                        //retrieve token and put it into request
                        String token = AppController.getString(getApplicationContext(), "token");
                        params.put("x-auth", token);
                        return params;
                    }

                };

                // Adding request to  queue
                AppController.getInstance().addToRequestQueue(jsonArrayRequest, tag_jsonArray_req);
            }
        });
    }

    // Set datepickers for EditText fields
    private void setDateTimeField() {
        dateFrom.setOnClickListener(this);
        dateTo.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateFrom.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateTo.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    // Listen for clicks on EditTexts
    @Override
    public void onClick(View view) {
        if(view == dateFrom) {
            fromDatePickerDialog.show();
        } else if(view == dateTo) {
            toDatePickerDialog.show();
        }
    }

    private void logoutUser() {
        session.setLogin(false);
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    //ADDED Start:
    private void showCalendar(){
        Intent myIntent = new Intent(Intent.ACTION_VIEW).setDataAndType(null, CalendarActivity.MIME_TYPE);
        startActivity(myIntent);
    }
    //ADDED End

    /*
    function to show dialog
     */
    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    /*
    function to hide dialog
     */
    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
