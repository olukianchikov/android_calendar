/*
 * Copyright (C) 2011 Chris Gao <chris@exina.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.androidsources.loginandregistration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.androidsources.loginandregistration.app.AppConfig;
import com.androidsources.loginandregistration.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CalendarActivity extends Activity  implements CalendarView.OnCellTouchListener{
	public static final String MIME_TYPE = "com.mcc.cursor.dir/com.mcc.calendar.date";
	CalendarView mView = null;
	TextView mHit;
	Handler mHandler = new Handler();
	private String __token_str = null;
	private ArrayList<Date> allEventsDates = null; // stores up to date list of dates that have at least one event
	private int __selectedDay = 0;
	private int __selectedMonth = 0;
	private int __selectedYear = 0;
 // Event object will be put here each time we add/delete event or retvieve events.
	 													// Updates will all be reflected here.
 	private EventsHolder allEventObjects = EventsHolder.getInstance();

	public static void eraseEvents(){
		EventsHolder.getInstance().eraseEvents();
	}


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        mView = (CalendarView)findViewById(R.id.calendar);
        mView.setOnCellTouchListener(this);
		this.__token_str = AppController.getString(getApplicationContext(), "token");

        if(getIntent().getAction().equals(Intent.ACTION_PICK))
        	findViewById(R.id.hint).setVisibility(View.INVISIBLE);
		retrieveAllEvents();
    }

	@Override
	public void onDestroy(){
		CalendarActivity.eraseEvents();
		super.onDestroy();
	}

	/** Called when the activity is resumed (entering the running state). */
	@Override
	public void onResume(){
		System.out.println("On Resume happened! -----------------------");
		updateCalendar();
		mView.refreshThisMonth();  // to update drawings.
		super.onResume();
	}


	private void updateCalendar(){
		if (allEventsDates != null) {
			System.out.println("****Number of PERSISTENT EVENTS:"+Integer.toString(allEventObjects.getData().size()));


			System.out.println("All events Dates are not null, now we are adding new items.");
			Boolean contains = false;
			ArrayList<Date> allEventsDatesCopy = new ArrayList<Date>(allEventsDates);
			for (Event ev : allEventObjects.getData()) {
				contains = false;
				//System.out.println("___ Comparing Persistent Event Date:"+removeTime(ev.get__date()).toString());
				for (Date theDate : allEventsDates ) {
					//System.out.println("___ with date for drawing:"+removeTime(theDate).toString());
					if (removeTime(theDate).equals(removeTime(ev.get__date()))){
						contains = true;
						break;
					}
				}
				if (!contains){
					System.out.println("______________ I FOUND something TO ADD!!!");
					allEventsDates.add(removeTime(ev.get__date()));
				}
			}

			int index = 0;
			for (Date theDate : allEventsDatesCopy) {
				contains = false;
				for (Event ev : allEventObjects.getData()) {
					if (removeTime(theDate).equals(removeTime(ev.get__date()))){
						contains = true;
						break;
					}
				}
				if (!contains){
					System.out.println("______________ I FOUND something TO Delete!!!");
					allEventsDates.remove(index);
				}
				index += 1;
			}
			showDates();
		}
	}

	private void retrieveAllEvents(){
		// send request for retrieval all data
		String tag_jsonArray_req = "retrieve_events"; // meaningless, having no purpose
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
				AppConfig.URL_EVENTS, new Response.Listener<JSONArray>() {

			@Override
			public void onResponse(JSONArray response) {
				Log.w("response", response.toString());
				// parse response to retrieve name and date for each event
				ArrayList<String> allEvents = new ArrayList<String>();
				for (int i = 0; i < response.length(); i++) {
					JSONObject event = null;
					String name = null;
					String date = null;
					String location = null;
					String event_desc = null;
					String id = null;
					String user = null;
					int priority = 0;
					String status = null;
					String availability = null;
					try {
						event = response.getJSONObject(i);
						id = event.getString("_id");
						name = event.getString("name");
						date = event.getString("date");
						user = event.getString("user");
						location = event.getString("loc");
						priority = event.getInt("priority");
						status = event.getString("sts");
						availability = event.getString("availability");
						event_desc = id + " " + name + " " + date + " " + user + " " + location;
						System.out.println("EVent description: " + event_desc);
					} catch (JSONException e) {
						e.printStackTrace();
					}

					allEvents.add(date);
					// NOW HERE I NEED TO DRAW ALL OF THESE EVENTS ON THE CALENDAR:
					allEventsDates = new ArrayList<Date>();
					SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					try {
						//simpleFormat.parse(date);
						// Add to the "Global" (singleton) storage of events.
						allEventObjects.addEvent(new Event(id, simpleFormat.parse(date), name, user, location, priority, status, availability));
							for(String item : allEvents){
								//System.out.println("Date in String:"+item);
								allEventsDates.add(simpleFormat.parse(item));
								//System.out.println("Date as Date:" + item);
							}
							//for(Date item : allEventsDates){
								//System.out.println("Date as Date:" + item.toString());
							//}
						} catch (ParseException e) {
							e.printStackTrace();
						}
						// add to the list here -- do Date manipulation.

				}
				showDates();
			}
		}, new Response.ErrorListener() {
			// In case there is a BAD RESPONSE
			@Override
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(getApplicationContext(),
						error.getMessage(), Toast.LENGTH_LONG).show();
			}
		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				HashMap<String, String> params = new HashMap<String, String>();
				//retrieve token and put it into request
				String token = AppController.getString(getApplicationContext(), "token");
				params.put("x-auth", token);
				return params;
			}

		};
		AppController.getInstance().addToRequestQueue(jsonArrayRequest, tag_jsonArray_req);
	}


	public void onTouch(Cell cell) {
		Intent intent = getIntent();
		String action = intent.getAction();
		System.out.println("Action is:"+action);
		if(action.equals(Intent.ACTION_PICK) || action.equals(Intent.ACTION_GET_CONTENT)) {
			int year  = mView.getYear();
			int month = mView.getMonth();
			int day   = cell.getDayOfMonth();
			
			// FIX issue 6: make some correction on month and year
			if(cell instanceof CalendarView.GrayCell) {
				// oops, not pick current month...				
				if (day < 15) {
					// pick one beginning day? then a next month day
					if(month==11)
					{
						month = 0;
						year++;
					} else {
						month++;
					}
					
				} else {
					// otherwise, previous month
					if(month==0) {
						month = 11;
						year--;
					} else {
						month--;
					}
				}
			}
			
			Intent ret = new Intent();
			ret.putExtra("year", year);
			ret.putExtra("month", month);
			ret.putExtra("day", day);
			this.setResult(RESULT_OK, ret);
			finish();
			return;
		}
		int day = cell.getDayOfMonth();
		/*if(mView.firstDay(day))
			mView.previousMonth();
		else if(mView.lastDay(day))
			mView.nextMonth();
		else {*/
			__selectedDay = day;
			__selectedMonth = mView.getMonth();
			__selectedYear = mView.getYear();
		//}
/*
		mHandler.post(new Runnable() {
			public void run() {
				Toast.makeText(CalendarActivity.this, DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " " + mView.getYear(), Toast.LENGTH_SHORT).show();
			}
		});*/
    }

	public void onLastClicked(View v){
		if(v.getId() == R.id.buttonLast){
			System.out.println("Last pressed!");
			mView.previousMonth();
			mHandler.post(new Runnable() {
				public void run() {
					Toast.makeText(CalendarActivity.this, DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " " + mView.getYear(), Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	public void onShowClicked(View v){
		if(v.getId() == R.id.buttonShowEvents){
			System.out.println("Show pressed!");
			if (mView.checkDayHasEvents(__selectedDay, __selectedMonth, __selectedYear)){
				// SHOW THE WINDOW WITH EVENTS.
				System.out.println("_________________YES, I WILL SHOW THE WINDOW WHEN IT IS READY___________");
				// Launching other activity to list day events
				Intent intent = new Intent(CalendarActivity.this, ListDayEvents.class);
				intent.putExtra("day", __selectedDay);
				intent.putExtra("month", __selectedMonth);
				intent.putExtra("year", __selectedYear);
				startActivity(intent);
			} else {
				mHandler.post(new Runnable() {
					public void run() {
						Toast.makeText(CalendarActivity.this, "There are no events on the chosen day", Toast.LENGTH_SHORT).show();
					}
				});
			}
		}
	}

	public void onCreateClicked(View v){
		if(v.getId() == R.id.buttonCreate){
			System.out.println("Create pressed!");
			// Launching other activity to list retrieved events
			Intent intent = new Intent(CalendarActivity.this, CreateEventActivity.class);
			intent.putExtra("day", __selectedDay);
			intent.putExtra("month", __selectedMonth);
			intent.putExtra("year", __selectedYear);
			startActivity(intent);
		}
	}



	public void onNextClicked(View v){
		if(v.getId() == R.id.buttonNext){
			System.out.println("Next pressed!");
			mView.nextMonth();
			mHandler.post(new Runnable() {
				public void run() {
					Toast.makeText(CalendarActivity.this, DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " " + mView.getYear(), Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	/*
	Should be used after successful adding a new event to the server
	 */
	public void addDate(Date newDate){
		if (allEventsDates != null){
			allEventsDates.add(newDate);
		}
	}

	/*
	Should be used after successful delete of a new event to the server
	 */
	public void removeDate(Date newDate){
		if (allEventsDates != null) {
			int num = allEventsDates.indexOf(newDate);
			allEventsDates.remove(num);
		}
	}

	private Date removeTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/*
	It will make list of dates available to initCells method in CalendarView.
	 */
	//TODO: Make sure the events are received first, before showing the calendar. Otherwise we get calendar empty, and after that we get events response.
	public void showDates(){
		if (allEventsDates != null){
			mView.setAllEvents(allEventsDates);
		}
	}


}