package com.androidsources.loginandregistration;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidsources.loginandregistration.app.AppConfig;
import com.androidsources.loginandregistration.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class CreateEventActivity extends Activity {
    private Button buttonCreate = null;
    private EditText event_name_input = null;
    private EditText event_date_input = null;
    private EditText event_time_input = null;
    private EditText event_location_input = null;
    private EditText event_priority_input = null;
    private Spinner event_availability_input = null;
    private Switch event_repeat_input = null;
    private Boolean errorDate = false;
    private Boolean errorTime = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        event_name_input = (EditText)findViewById(R.id.event_name_input);
        event_date_input = (EditText)findViewById(R.id.event_date_input);
        event_time_input = (EditText)findViewById(R.id.event_time_input);
        event_time_input.setText("12:00");
        event_location_input = (EditText)findViewById(R.id.event_location_input);
        event_priority_input = (EditText)findViewById(R.id.event_priority_input);
        event_availability_input = (Spinner)findViewById(R.id.event_availability_input);
        event_repeat_input = (Switch)findViewById(R.id.event_repeat_input);
        buttonCreate = (Button) findViewById(R.id.buttonCreate);


        Intent intent = getIntent();
        int preselectedDay = intent.getIntExtra("day", 0);
        int preselectedMonth = intent.getIntExtra("month", 0);
        int preselectedYear = intent.getIntExtra("year", 0);

        Date preselectedDate = getDate(preselectedDay, preselectedMonth, preselectedYear);
        event_date_input.setText(getDateAsString(preselectedDate));
        event_time_input.setText(getTimeAsString(preselectedDate));

        event_date_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!event_date_input.getText().toString().matches("([0-3][0-9]?\\.[0-1][0-9]?\\.[2][0][0-9][0-9])")) {
                    errorDate = true;
                } else {
                    errorDate = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        event_time_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!event_time_input.getText().toString().matches("([0-2][0-9]\\:[0-5][0-9])"))
                {
                    errorTime = true;
                } else {
                    errorTime = false;
                }

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        });


        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInfo()){
                    if (errorDate){
                        // Show error message
                        Toast.makeText(getApplicationContext(),
                                "Date must be in dd.MM.yyyy", Toast.LENGTH_LONG).show();
                    } else if (errorTime){
                        // Show error message
                        Toast.makeText(getApplicationContext(),
                                "Time must be in HH:MM", Toast.LENGTH_LONG).show();
                    }else {
                        // send request and add new Event into singleton.
                        String name = event_name_input.getText().toString();
                        String date = event_date_input.getText().toString();
                        String time = event_time_input.getText().toString();
                        String location = event_location_input.getText().toString();
                        String priority = event_priority_input.getText().toString();
                        String availability = event_availability_input.getSelectedItem().toString();
                        Boolean repeat = event_repeat_input.isSelected();
                        // TODO: make status proper
                        //String name, String loc, Boolean repeat, String sts, String priority, String availability, String date, String time
                        Date myDate = getDateFromDateTimeStrings(date, time);
                        sendPostRequest(AppConfig.getUrlEventCreate(), "create_event", makeJsonString(name, location, repeat, "upcoming", priority, availability, myDate.toString(), time));
                    }
                } else {
                    // Show error message
                    Toast.makeText(getApplicationContext(),
                            "Not all fields are valid", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private Boolean validateInfo(){
        if (event_name_input.length() < 2) return false;
        if (event_location_input.length() < 3) return false;
        if (event_date_input.length() < 10) return false;
        if (event_time_input.length() < 5) return false;
        if (event_priority_input.length() < 1) return false;
        if (!isInteger(event_priority_input.getText().toString())) return false;
        return true;
    }

    private boolean isInteger( String input ) {
        try {
            int val = Integer.parseInt( input );
            if ((val < 1) || (val >10) ) return false;
                else return true;
        }
        catch( Exception e ) {
            return false;
        }
    }

    private String getDateAsString(Date myDate){
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        return df.format(myDate);
    }

    private String getTimeAsString(Date myDate){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        return df.format(myDate);
    }

    private Date getDateFromDateTimeStrings(String myDate, String myTime){
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        try {
            return df.parse(myDate +" "+ myTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }


    private Date getDate(int day, int month, int year){
        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }

    private String makeJsonString(String name, String loc, Boolean repeat, String sts, String priority, String availability, String date, String time){
        String myRepeat = null;
        if (repeat) myRepeat = "true";
                else myRepeat = "false";
        return "{ \"name\":\"" + name +"\", \"loc\":\"" + loc +"\", \"date\":\"" + date +"\", \"time\":\"" +time+"\", \"repeat\":" + myRepeat +", \"sts\":\"" + sts+"\", \"priority\":" + priority +", \"availability\":\"" + availability + "\"}";
    }

    private void sendPostRequest(String url, String action, String jSonData){
        String tag_jsonArray_req = action;
        System.out.println("CREATE URL:"+url);
        System.out.println("CREATE action:"+action);
        System.out.println("CREATE json:"+jSonData);
        // send request for retrieval all data
        JsonObjectRequest jsonObjectRequest = null;
        try {
            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    url, new JSONObject(jSonData), new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    System.out.println("response"+ response.toString());
                    // parse response to retrieve name and date for each event
                    ArrayList<String> allEvents = new ArrayList<String>();
                    //for (int i = 0; i < response.length(); i++) {
                        JSONObject event = null;
                        String name = null;
                        String date = null;
                        String location = null;
                        String event_desc = null;
                        String id = null;
                        String user = null;
                        int priority = 0;
                        String status = null;
                        String availability = null;
                        try {
                            event = response;
                            id = event.getString("_id");
                            name = event.getString("name");
                            date = event.getString("date");
                            user = event.getString("user");
                            location = event.getString("loc");
                            priority = event.getInt("priority");
                            status = event.getString("sts");
                            availability = event.getString("availability");
                            event_desc = id + " " + name + " " + date + " " + user + " " + location;
                            System.out.println("Created EVent description: " + event_desc);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        simpleFormat.setTimeZone(TimeZone.getTimeZone("Europe/Helsinki"));
                        try {
                            Date dateToInsert = simpleFormat.parse(date);

                            Calendar cal = Calendar.getInstance(); // creates calendar
                            cal.setTime(dateToInsert); // sets calendar time/date
                            cal.add(Calendar.HOUR_OF_DAY, 2); // adds two hours
                            dateToInsert = cal.getTime(); // returns new date object

                            EventsHolder.getInstance().addEvent(new Event(id, dateToInsert, name, user, location, priority, status, availability));
                            System.out.println("Created EVent description: " + dateToInsert.toString());
                            Thread thread = new Thread(){
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(2000); // As I am using LENGTH_LONG in Toast
                                        setResult(RESULT_OK, null);
                                        CreateEventActivity.this.finish();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            };

                            Toast.makeText(getApplicationContext(),
                                    "Successfully added " + name, Toast.LENGTH_SHORT).show();
                            thread.start();
                            CreateEventActivity.this.finish();
                        } catch (ParseException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        // adding to the singleton events:
                   // }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),
                            error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<String, String>();
                    //retrieve token and put it into request
                    String token = AppController.getString(getApplicationContext(), "token");
                    params.put("x-auth", token);
                    return params;
                }

            };
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    e.getMessage(), Toast.LENGTH_LONG).show();
        }

        // Adding request to  queue
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_jsonArray_req);
    }
}
